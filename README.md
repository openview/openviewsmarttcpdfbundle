OpenviewSmartTCPDFBundle
========================


## Installation

Install TCPDF library in your php environment (note that this step may not be mandatory, still not verified ;-) )

    apt-get install php-tcpdf

manually copy the tcpdf library in the vendors. The path must be:

    vendor/tcpdf/tcpdf/tcpdf.pdf

Add this repository in your composer.json

    "repositories": [
        { "type": "git", "url": "git@github.com:OPENVIEW-DEV/OpenviewSmartTCPDFBundle.git" }
    ],
    "require": {
        "openview/smart-tcpdf-bundle": "2.*"
    }


## Usage

In a controller, add these lines:

    $pdfObj = $this->container->get("openview.tcpdf")->create();
    $pdfObj->AddPage();
    $pdfObj->writeHTMLCell(100, 0, $x='', $y='', $htmlSectionCode, 0, 1, 0, true, 'L', true);
    $pdfObj->Output('filename.pdf', "I");